# How to manually flash Open-Mesh access points

If you are interested in flashing Ubiquiti UBNT-AC-LR, which is supported - look at UBNT_AC_LR_flash.md - https://gitlab.com/pbcom_bobac/altiwi-doc/blob/4d577e428642f81a0ec886a8cbc2a6d73ea1fd3f/UBNT_AC_LR_flash.md

## Prerequisities
First, you need to know, what kind of hardware do you own. Altiwi supports most of the Open-Mesh
access points from OM2 onwards. Old MR500 devices are not supported. Other important topic is
wether you own 2.4 GHZ only or 5GHz device. The reason is that all devices that use 5GHz radio
are locked and it is not possible to reflash them without further unlocking process (which will be
described in this document too).

You also need the access to your Cloudtrax account and you the access points you want to reflash
must be communicating with the Cloudtrax console in case they posses 5GHz radio. If you have only
2.4 GHz access points, Cloudtrax is not mandatory as it is possible to reflash them without root
access (that is gained via Cloudtrax > Advanced > Root password).

### Supported devices - no need for unlock
* OM2P-LC
* OM2P
* OM2P-HS

### Supported devices - need unlock
* OM5P-AC
* MR900
* MR1750
* A40
* A60
* A42
* A62 _Warning: needs different unlock procedure!_

## Unlock procedure (only for 5GHz radios, skip if using 2.4GHz only)
First, you need to get the local IP address of the access point you want to unlock. This addresscan be obtained from your Cloudtrax account in Manage > Access points. We advise you to have a wired connection to your network and also recommend to have the access point connected to the wired connection(not use the repeater/mesh node). Technically it is possible to unlock the repeater node if you are able to ssh into it, but we believe that it is quite pointless as this procedure describes manual reflashing process which implies use of wired connection in the next step.

Once you have the IP address of your device, we recommend you to test if you can access it via the ping command, eg. `ping 192.168.1.101` and see the reply.

Then you should navigate to Advanced > Root password and use the Show button to reveal the devices' password. This password is common for all devices in your Cloudtrax network.

Once you have the password, connect to the device:

`ssh root@192.168.1.101` and use the password.

Note 1: Replace the "192.168.1.101" with your own address.
Note 2: If you are using Windows, you might want to use putty from http://www.putty.org

After login, issue following commands (You might copy & paste, putty uses right mouse button for pasting:

### Unlock procedure for all 5GHz capable access points EXCEPT A62

```
RSA_KEY_HEADER_SIZE=0x20
RSA_KEY_OFFSET=0x8000
ART_PARTITION=mtd7
BYTES=$((RSA_KEY_HEADER_SIZE))
SEEK=$(($RSA_KEY_OFFSET/$RSA_KEY_HEADER_SIZE))

dd if=/dev/zero bs=$BYTES count=1 | dd of=/dev/$ART_PARTITION bs=$BYTES seek=$SEEK count=1 conv=notrunc
reboot && exit
```

### Unlock procedure ONLY for A62

```
RSA_KEY_HEADER_SIZE=0x20
RSA_KEY_OFFSET=0x0
ART_PARTITION=mtd9
BYTES=$((RSA_KEY_HEADER_SIZE))
SEEK=$(($RSA_KEY_OFFSET/$RSA_KEY_HEADER_SIZE))

dd if=/dev/zero bs=$BYTES count=1 | dd of=/dev/$ART_PARTITION bs=$BYTES seek=$SEEK count=1 conv=notrunc
reboot && exit
```

The connection will be terminated and the access point will reboot. After reboot, the device will work as normally, but will be able to receive alternative firmware. Before you proceed, we recommend you to verify, that the device is working normally.

## Flash the device
To successfully flash the device you need three files:
1. WinPCAP utility. If you use for example WireShark you may have this installed. If not, install this utility BEFORE flashing firmware - https://s3-eu-west-1.amazonaws.com/cloudrobot-fw/WinPcap_4_1_3.exe 
2. Flash utility from OpenMesh. It is a classic OM flashing utility and can be also used for reverting back to stock OM firmware - https://s3-eu-west-1.amazonaws.com/cloudrobot-fw/open-mesh-flash-ng.exe
3. The firmware. The firmware is device dependent, please choose a propper one that suits your access point. For OM2P series access points, the firmware itself is the same for OMP2-LC, OM2P, OM2P-HS. Latest firmwares can be downloaded from https://beta.altiwi.com/download. Please use the link in Download column.
3. Open command line and `cd` to the directory you have put your firmware and flash utility.
4. Run `open-mesh-flash-ng` without parameters and watch for the output. The utility should print all network devices detected in your computer. Locate the one which corresponds to your ethernet adapter. It will probably have the "ethernet" in it's name. eg. "Intel gigabit ethernet adapter", or something simmilar.
5. Let's say that your ethernet adapter have been referred under index "1". in such case issue following command: `open-mesh-flash-ng 1 your-firmaware.bin`. If you ommit the last parameter, e.g.: `open-mesh-flash-ng 1`, you will reflash the access point to original Open-Mesh firmware.
6. Connect the ethernet cable to your access point and direct to the ethernet adapter of your computer. After that (!) connect the power to your access point and wait for the flashing process. The device is reflashed after you see the `Device ready for unplug` message. Unplug power, ethernet and now you can use the device with Cloudrobot (Altiwi's clound management console, just like Cloudtrax for OM was).
7. In case that you do not have any power adapter (or the device is even not equipped with one, like Axx series), you need 2 ethernet cables and the power injector. First connect the device and the injector, then the injector and your computer (be carefull to use the powered side of the injector on the proper port of the access point) and then connect the power, not vice versa. The point is that the device asks for firmware in some 2 seconds window just after powering up, so the wiring must be set up before you turn the power on.

## How to get back to the original Open-Mesh firmware
If you need, for any reason, to return your original Open-Mesh firmware, just follow their manual reflash procedure as outlined on their support website. The device will work with no issues even in case it has been unlocked. If you'd want to reflash the device back to Altiwi's firmware, you do not need to follow the unlocking procedure again. Just flash the device.

## Disclaimer
Although the procedure of reflashing the firmware is very robust and the author of this document performed it hunderts times, it is, however, quite delicate process, which can lead to permanent damage of your device. If you are not comfortable with it, please don't do it, or at least use uninterruptible power supply to avoid power outage during the flashing/unlocking process. If anything ever happened to any of such devices during flashing process, the power outage was probably the most probably the root cause. Others might be any other reason of computer failures during the process etc.
Altiwi does not make any kind of warranties, you are doing this process at your own risk!
