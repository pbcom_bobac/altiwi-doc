# Flashing Ubiquiti UBNT-AC-LR

Open an SSH session to the original firmware (User: ubnt, Pswd: ubnt). If no DHCP server is running, the AP will have the IP 192.168.1.20, so it is recommended to power - up the system with PoE injector only and to connect it directly to your computer. In such case you will need to set up your ethernet adapter IP from the same range, e.g. 192.168.1.21, mask 255.255.255.0, no gw or DNS needed. If this is no option, please use Ubiquiti's UniFi Discover utility to look up your AP's IP.

Download latest firmware from Altiwi console and copy it to /tmp using scp:

```scp ubntaclr-sysupgrade.bin ubnt@192.168.1.20:/tmp/```
These devices come with a flip-flop flash layout. The bootloader checks the bs partition to determine whether to boot kernel0 or kernel1. If one partition holds a signed image and the other does not it will always prefer the signed one. That's why both partitions need to be written.

## Note
On one occasion it was impossible to connect to the device via SCP with pscp program which is a part of PuTTY. We'd recommand using Windows 10's OpenSSH. Install it according to instructions at https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse.
In such case the ```scp ubntaclr-sysupgrade.bin ubnt@192.168.1.20:/tmp/``` should work just fine. If it is not possible and you are using for example older Windows, you can just connect the UBNT-AC-LR device to the local network with internet access and download the firmware directly to the device. Here is the step-by-step:

1. Copy the link address from the Altiwi console for the latest firmware
2. Use some ssh client (e.g. PuTTY) to connect to the device as user ubnt, password ubnt
3. Download the firmware directly to the device via curl:
```
cd /tmp
curl -L https://beta.altiwi.com....[the link copied from the console] > ubntaclr-sysupgrade.bin
```
4. then follow the instructions bellow:

Connect to the device at ubnt@192.168.1.20 and run the following commands to replace both firmware partitions with OpenWrt:

* If mtd is missing on your device downgrade the stock firmware to a version before march 2017.

* For the ac pro going down to 3.7.58 was sufficient.

```
BZ.v3.7.40# mtd write /tmp/ubntaclr-sysupgrade.bin kernel0
Unlocking kernel0 ...
Erasing kernel0 ...
Writing from /tmp/ubntaclr-sysupgrade.bin to kernel0 ...  [e/w]

BZ.v3.7.40# mtd erase kernel1
Unlocking kernel1 ...
Erasing kernel1 ...
```
Then write a nullbyte to the bs partition (check /proc/mtd for the correct path) to make it boot from kernel0, which is the partition that sysupgrade writes to.
```
BZ.v3.9.19# cat /proc/mtd 
dev:    size   erasesize  name
mtd0: 00060000 00010000 "u-boot"
mtd1: 00010000 00010000 "u-boot-env"
mtd2: 00790000 00010000 "kernel0"
mtd3: 00790000 00010000 "kernel1"
mtd4: 00020000 00010000 "bs"
mtd5: 00040000 00010000 "cfg"
mtd6: 00010000 00010000 "EEPROM"

BZ.v3.9.19# dd if=/dev/zero bs=1 count=1 of=/dev/mtd4
1+0 records in
1+0 records out
1 byte copied, 7.3437e-05 s, 13.6 kB/s
```
After a reboot the device will acqiuire IP address from DHCP and connect to Altiwi dashboard after some 10-15 minutes.

* When bootselect is not written correctly there is a 50/50 chance that the device will be booting the kernel from kernel1, but the rootfs from kernel0, which leads to kernel panics when the kernel ABI changes.

## Credits
The above instructions are slightly modified from OpenWrt's wiki - please consult https://openwrt.org/toh/ubiquiti/unifiac if you need more info.